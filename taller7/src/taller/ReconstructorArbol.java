package taller;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import taller.interfaz.IReconstructorArbol;



public class ReconstructorArbol implements IReconstructorArbol
{
	private String[] preorden;
	private String[] inorden;
	private ArbolSimple arbol;
	
	public ReconstructorArbol() 
	{
		arbol= new ArbolSimple();
	}

	@Override
	public void cargarArchivo(String nombre) throws IOException {

		Properties prop = new Properties();

		InputStream input = new FileInputStream(nombre);

		// load a properties file
		prop.load(input);

		// get the property value and print it out
		preorden= prop.getProperty("preorden").split(",");
		inorden= prop.getProperty("inorden").split(",");

	}

	@Override
	public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException {
		try(FileWriter file = new FileWriter(info))
		{
			file.write(arbol.aJson().toString());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}


	}

	@Override
	public void reconstruir() {
		arbol.reconstruirse(preorden, inorden);
		System.out.println("preorden");
		System.out.println(arbol.darPreorden());
		System.out.println("inorden");
		System.out.println(arbol.darInorden());
	}

}
