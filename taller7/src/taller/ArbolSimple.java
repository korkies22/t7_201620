package taller;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.sun.org.apache.bcel.internal.generic.IfInstruction;
import com.sun.org.apache.xml.internal.dtm.ref.DTMDefaultBaseIterators.PrecedingIterator;
public class ArbolSimple<E>
{
	private Nodo<E> raiz;

	public ArbolSimple()
	{
		raiz= null;
	}

	private class Nodo<E>
	{
		private Nodo<E> izquierdo;
		private Nodo<E> derecho;
		private E data;
		public Nodo(E pData)
		{
			data=pData;
			izquierdo=null;
			derecho=null;
		}

		public void insertarIzquierda(E pData)
		{
			izquierdo= new Nodo(pData);
		}

		public void insertarDerecha(E pData)
		{
			derecho= new Nodo(pData);
		}



		public String darPreorden()
		{
			String resp= "";
			resp=resp.concat(data.toString());
			if(izquierdo!=null)
			{
				resp= resp.concat(izquierdo.darPreorden());
			}
			if(derecho!=null)
			{
				resp= resp.concat(derecho.darPreorden());
			}
			return resp;
		}

		public String darInorden()
		{
			String resp= "";
			if(izquierdo!=null)
			{
				resp= resp.concat(izquierdo.darInorden());
			}
			resp=resp.concat(data.toString());
			if(derecho!=null)
			{
				resp= resp.concat(derecho.darInorden());
			}
			return resp;
		}

		public JSONObject aJson() 
		{
			try
			{
				JSONObject j = new JSONObject();
				if(derecho!=null)
				{
					j.put("derecha", derecho.aJson());
				}
				j.put("nodo",data+"");
				if(izquierdo!=null)
				{
					j.put("izquierda", izquierdo.aJson());
				}
				return j;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return null;

		}

		public void reconstruirse(E[] preorden, int preStart, int preEnd, E[] inorden, int inStart, int inEnd) 
		{
			//find parent element index from inorder
			int k=inStart;
			while(k<inEnd)
			{
				if(inorden[k].equals(preorden[preStart]))
					break;
				k++;
			}
			if(k>inStart)
			{
				izquierdo= new Nodo<>(preorden[preStart+1]);
				if(inStart<k-1)
				{
					izquierdo.reconstruirse(preorden, preStart+1, preStart+(k-inStart), inorden, inStart, k-1);
				}
			}
			if(k<inEnd)
			{
				derecho= new Nodo<>(preorden[preStart+(k-inStart)+1]);
				if(inEnd>k+1)
				{
					derecho.reconstruirse(preorden, preStart+(k-inStart)+1, preEnd, inorden, k+1 , inEnd);
				}
			}

		}

		public boolean contieneSubArbol(Nodo<E> act) 
		{
			if(act==null) return false;
			boolean izq=true;
			boolean der=true;
			if(this.data.equals(act.data))
			{
				if(izquierdo!=null)
				{
					izq=izquierdo.contieneSubArbol(act.izquierdo);
				}
				else
					izq= act.izquierdo==null;
				if(derecho!=null)
				{
					der=derecho.contieneSubArbol(act.derecho);
				}
				else
					der= act.derecho==null;
				return izq&&der;
			}
			else
			{
				izq=false;
				der=false;
				if(izquierdo!=null)
				{
					izq=izquierdo.contieneSubArbol(act);
				}
				if(derecho!=null)
				{
					der=derecho.contieneSubArbol(act);
				}
				return izq||der;
			}
			
		}
	}

	public void reconstruirse(E[] preorden, E[] inorden) 
	{
		raiz=new Nodo(preorden[0]);
		raiz.reconstruirse(preorden,0,preorden.length-1 ,inorden, 0, inorden.length-1);
	}

	public String darPreorden()
	{
		return raiz.darPreorden();
	}

	public String darInorden()
	{
		return raiz.darInorden();
	}

	public JSONObject aJson() 
	{
		return raiz.aJson();
	}
	
	public boolean contieneSubArbol(ArbolSimple<E> sub)
	{
		return raiz.contieneSubArbol(sub.raiz);
	}
}
